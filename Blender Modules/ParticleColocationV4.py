bl_info = {    
    "name": "Particles Colocation",    
    "category": "Object",
}

import bpy
import time
import math
import random
import numpy as np
from scipy import interpolate
from scipy.interpolate import spline

class ParticlesColocation(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.move_x"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Colocation"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

        numero_objetos = 4
                       
        #f = open('/Users/edgarfigueiras/Documents/TFG DM/Blender Modules/datos_particles.txt', 'r')
        #for line in f: 
        #for x in range(N):
        	#r = random.uniform(-10,10);
	        #if random.uniform(0, 1) < gaussV2(r):
		       #array_resultados.append(r)

        #Reading the data to generate the function who originated it
        file_with_data = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/dataToInterpolate1.txt', 'r')
        x_data_file = []
        y_data_file = []

        for line in file_with_data: 
            xData,yData = line.split(" ") 
            x_data_file.append(float(xData))
            y_data_file.append(float(yData))

        #Order the two lists mantaining  their correlation
        x_data_file, y_data_file = (list(t) for t in zip(*sorted(zip(x_data_file, y_data_file))))

        #Interpolation straight, creating the function to generate the points
        funcInter = interpolate.interp1d(x_data_file, y_data_file)


        for x in range(0,numero_objetos):
            emitter = bpy.data.objects[x+2]
            psys1 = emitter.particle_systems[-1]                 
            for pa in psys1.particles:
                #Añadiendo los datos del fichero a 3 variables 
                if x == 0 : 
                    centro = random.uniform(-2,0);
                if x == 1 : 
                    centro = random.uniform(-1,1);
                if x == 2 : 
                    centro = random.uniform(0,3);
                if x == 3 : 
                    centro = random.uniform(3,6);
                    
                x_pos = (random.uniform(-5,5))
                y_pos = (random.uniform(-5,5))
                z_pos = (funcInter(centro))
                
                pa.location = (x_pos,y_pos,z_pos)   
                #Refresh the graphics to show the changes at realtime          
                bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)  
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
        f.close()

def register():
    bpy.utils.register_class(ParticlesColocation)


def unregister():
    bpy.utils.unregister_class(ParticlesColocation)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   