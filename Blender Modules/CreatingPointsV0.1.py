bl_info = {    
    "name": "Spheres Creation",    
    "category": "Object",
}

import bpy
import time
import math

class SpheresCreation(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.move_x"           # unique identifier for buttons and menu items to reference.
    bl_label = "Spheres Creation"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

        #Borrar todos los objetos de la escena
        #bpy.ops.object.mode_set(mode='OBJECT')
        #bpy.ops.object.select_by_type(type = 'MESH')
        #bpy.ops.object.delete(use_global=False)
        #for item in bpy.data.meshes:
           # bpy.data.meshes.remove(item)
      
        f = open('/Users/edgarfigueiras/Documents/TFG DM/Blender Modules/datos.txt', 'r')
        # The original script
        #scene = context.scene
        #Sun = bpy.data.objects[2]
        #R = 32 # Numero total de movimientos 
        for line in f:
             
            xx,yy,zz = line.split(" ")
            me = bpy.data.meshes.new("Figura")
            ob = bpy.data.objects.new("Esfera", me)
            scn = context.scene      # get the current scene
            ob.location = (float(xx), float(yy), float(zz))
            ob.show_name = True
            
            scn.objects.link(ob)
            scn.objects.active = ob
            ob.select = True
            #ob.setLocation (0.0, -5.0, 1.0)       # position the object in the scene
     
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1) #Actualiza el estado gráfico para apreciar la animacion

        me.from_pydata(verts, [], faces)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
        f.close()

def register():
    bpy.utils.register_class(SpheresCreation)


def unregister():
    bpy.utils.unregister_class(SpheresCreation)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   