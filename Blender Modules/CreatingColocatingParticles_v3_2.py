bl_info = {    
    "name": "Particles Creation from Spheres",    
    "category": "Object",
}

import bpy
import time
import math
import random
import numpy as np
from scipy import interpolate
from scipy.interpolate import spline

class ParticlesCreationFromSpherer(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.creation"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Creation from Spheres"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.


        #PARTE DE COLOCACION
        #Reading the data to generate the function who originated it
        file_with_data = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/dataToInterpolate3.txt', 'r')
        x_data_file = []
        y_data_file = []

        for line in file_with_data: 
            xData,yData = line.split(" ") 
            x_data_file.append(float(xData))
            y_data_file.append(float(yData))

        #Order the two lists mantaining  their correlation
        x_data_file, y_data_file = (list(t) for t in zip(*sorted(zip(x_data_file, y_data_file))))

        #Interpolation straight, creating the function to generate the points
        funcInter = interpolate.interp1d(x_data_file, y_data_file)


        file_with_data2 = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/dataToInterpolate2.txt', 'r')
        x_data_file2 = []
        y_data_file2 = []

        for line in file_with_data2: 
            xData2,yData2 = line.split(" ") 
            x_data_file2.append(float(xData2))
            y_data_file2.append(float(yData2))

        #Order the two lists mantaining  their correlation
        x_data_file2, y_data_file2 = (list(t) for t in zip(*sorted(zip(x_data_file2, y_data_file2))))

        #Interpolation straight, creating the function to generate the points
        funcInter2 = interpolate.interp1d(x_data_file2, y_data_file2)

        #Reading the data to generate the function who originated it
        file_with_data3 = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/gaussSimple.txt', 'r')
        x_data_file3 = []
        y_data_file3 = []

        for line in file_with_data3: 
            xData3,yData3 = line.split(" ") 
            x_data_file3.append(float(xData3))
            y_data_file3.append(float(yData3))

        #Order the two lists mantaining  their correlation
        x_data_file3, y_data_file3 = (list(t) for t in zip(*sorted(zip(x_data_file3, y_data_file3))))

        #Interpolation straight, creating the function to generate the points
        funcInter3 = interpolate.interp1d(x_data_file3, y_data_file3)
        
        numeroObjeto = 1         

        for x in range(0,numeroObjeto):
            if x == 0 : 
                emitter = bpy.data.objects['Sphere']
            if x == 1 :
                emitter = bpy.data.objects['Sphere.001']

                
            psys1 = emitter.particle_systems[-1]                 
            for pa in psys1.particles:
                #Añadiendo los datos del fichero a 3 variables 
                centroX = 0
                centroY = 0
                centroZ = 0
                if x == 0 : 
                    while centroX == 0:
                        r = random.uniform(min(x_data_file),max(x_data_file));
                        if random.uniform(0, 1) <= funcInter(r):
                            centroX = r
                    while centroY == 0:
                        r = random.uniform(min(x_data_file),max(x_data_file));
                        if random.uniform(0, 1) <= funcInter(r):
                            centroY = r
                    while centroZ == 0:
                        r = random.uniform(min(x_data_file),max(x_data_file));
                        if random.uniform(0, 1) <= funcInter(r):
                            centroZ = r
                if x == 1 : 
                    while centroX == 0:
                        r = random.uniform(min(x_data_file3),max(x_data_file3));
                        if random.uniform(0, 1) <= funcInter3(r):
                            centroX = r
                    while centroY == 0:
                        r = random.uniform(min(x_data_file3),max(x_data_file3));
                        if random.uniform(0, 1) <= funcInter3(r):
                            centroY = r
                    while centroZ == 0:
                        r = random.uniform(min(x_data_file3),max(x_data_file3));
                        if random.uniform(0, 1) <= funcInter3(r):
                            centroZ = r
                    
                x_pos = (centroX)
                y_pos = (centroY)
                z_pos = (centroZ)
                
                pa.location = (x_pos,y_pos,z_pos)   
                #Refresh the graphics to show the changes at realtime          
                #bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)  

        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        #Avanzar en la escena para mostrar los cambios al renderizar OBLIGATORIO
        bpy.context.scene.frame_current = bpy.context.scene.frame_current + 10
        file_with_data.close()
        file_with_data2.close()
        return {'FINISHED'}            # this lets blender know the operator finished successfully.

        #me.from_pydata(verts, [], faces)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.

def register():
    bpy.utils.register_class(ParticlesCreationFromSpherer)


def unregister():
    bpy.utils.unregister_class(ParticlesCreationFromSpherer)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   
