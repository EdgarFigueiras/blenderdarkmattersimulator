bl_info = {    
    "name": "Particles Colocation",    
    "category": "Object",
}

import bpy
import time
import math
import random
import numpy as np
from scipy import interpolate
from scipy.interpolate import spline

class ParticlesColocation(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.colocation"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Colocation"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

        numero_objetos = 4
                       
        #f = open('/Users/edgarfigueiras/Documents/TFG DM/Blender Modules/datos_particles.txt', 'r')
        #for line in f: 
        #for x in range(N):
        	#r = random.uniform(-10,10);
	        #if random.uniform(0, 1) < gaussV2(r):
		       #array_resultados.append(r)

        #Reading the data to generate the function who originated it
        file_with_data = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/dataToInterpolate2.txt', 'r')
        x_data_file = []
        y_data_file = []

        for line in file_with_data: 
            xData,yData = line.split(" ") 
            x_data_file.append(float(xData))
            y_data_file.append(float(yData))

        #Order the two lists mantaining  their correlation
        x_data_file, y_data_file = (list(t) for t in zip(*sorted(zip(x_data_file, y_data_file))))

        #Interpolation straight, creating the function to generate the points
        funcInter = interpolate.interp1d(x_data_file, y_data_file)


        file_with_data2 = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/dataToInterpolate3.txt', 'r')
        x_data_file2 = []
        y_data_file2 = []

        for line in file_with_data2: 
            xData2,yData2 = line.split(" ") 
            x_data_file2.append(float(xData2))
            y_data_file2.append(float(yData2))

        #Order the two lists mantaining  their correlation
        x_data_file2, y_data_file2 = (list(t) for t in zip(*sorted(zip(x_data_file2, y_data_file2))))

        #Interpolation straight, creating the function to generate the points
        funcInter2 = interpolate.interp1d(x_data_file2, y_data_file2)


        for x in range(0,numero_objetos):
            emitter = bpy.data.objects[x+2]
            psys1 = emitter.particle_systems[-1]                 
            for pa in psys1.particles:
                #Añadiendo los datos del fichero a 3 variables 
                centro = 0
                if x == 0 : 
                    while centro == 0:
                        r = random.uniform(min(x_data_file),max(x_data_file));
                        if random.uniform(0, 1) < funcInter(r):
                            centro = r
                if x == 1 : 
                    while centro == 0:
                        r = random.uniform(min(x_data_file2),max(x_data_file2));
                        if random.uniform(0, 1) < funcInter2(r):
                            centro = r
                if x == 2 : 
                    while centro == 0:
                        r = random.uniform(min(x_data_file),max(x_data_file));
                        if random.uniform(0, 1) < funcInter(r):
                            centro = r+30
                if x == 3 : 
                    while centro == 0:
                        r = random.uniform(min(x_data_file2),max(x_data_file2));
                        if random.uniform(0, 1) < funcInter2(r):
                            centro = r+30
                    
                x_pos = (centro)
                y_pos = (random.uniform(-10,10))
                z_pos = (random.uniform(-10,10))
                
                pa.location = (x_pos,y_pos,z_pos)   
                #Refresh the graphics to show the changes at realtime          
                #bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)  
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        #Avanzar en la escena para mostrar los cambios al renderizar OBLIGATORIO
        bpy.context.scene.frame_current = bpy.context.scene.frame_current + 1
        file_with_data.close()
        file_with_data2.close()
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
        

def register():
    bpy.utils.register_class(ParticlesColocation)


def unregister():
    bpy.utils.unregister_class(ParticlesColocation)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   