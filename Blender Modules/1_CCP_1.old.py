bl_info = {    
    "name": "Particles Creation from Spheres",    
    "category": "Object",
}

import bpy
import time
import math
import random
import numpy as np
from scipy import interpolate
from scipy.interpolate import spline

class ParticlesCreationFromSpheres(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "particle.generator"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Creation from Spheres"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    #@staticmethod
    def execute(self,context):        # execute() is called by blender when running the operator.

        #Definir un material
        def makeMaterial(name, diffuse, specular, alpha):
            mat = bpy.data.materials.new(name)
            mat.diffuse_color = diffuse
            mat.type = 'HALO'
            mat.halo.size = 4   #Moddifies the Halo size of the particles
            mat.diffuse_intensity = 0
            mat.specular_color = specular
            mat.specular_intensity = 0.1
            mat.alpha = alpha
            mat.ambient = 0.1
            return mat
        
        
        #Materials created in function of their colour
        red = makeMaterial('Red',(1,0,0),(1,1,1),1)
        green = makeMaterial('Green',(0,1,0),(1,1,1),1)
        blue = makeMaterial('Blue',(0,0,1),(1,1,1),1)
        turqoise = makeMaterial('Turqoise',(0.1,0.9,0.9),(1,1,1),0.05)
        purple = makeMaterial('Purple',(0.8,0.4,0.8),(1,1,1),0.05)
        darkblue = makeMaterial('DarkBlue',(0.2,0.3,0.5),(1,1,1),0.05)
        gold = makeMaterial('Gold',(0.7,0.6,0.5),(1,1,1),0.05)
        darkred = makeMaterial('DarkRed',(0.4,0.1,0.2),(1,1,1),0.05)
        softgreen = makeMaterial('SoftGreen',(0.2,0.5,0.1),(1,1,1),0.05)
        bluespecial = makeMaterial('BlueSpecial',(0.3,0.2,0.7),(1,1,1),0.05)


    
        #Creation Part -------------
              
        path = bpy.data.scenes['Scene'].my_tool.path #Origin from where the data will be readen, selected by the first option in the Panel
        file_with_data = open(path, 'r')

        #Creating a file to save the particles placement information, helps to avoid a problem with Blender wich moves particles random
        file_with_particles_data = open(path + '.particles_info.txt', 'w+')


        numeroTotalObjetos = file_with_data.readline()    #Number of objects that will be used as particle emitters 
        numeroObjeto = 000  #number of objects that will be used as particles emitters

        file_with_particles_data.write(numeroTotalObjetos)

        for numeroObjeto in range(0,int(numeroTotalObjetos)):
             
            placement_data =  file_with_data.readline()
            xx,yy,zz = placement_data.split(" ")  #Adds the axis information of particle generator object placement

            #Set the background color to black
            bpy.context.scene.world.horizon_color = (0, 0, 0)

            
            #Creation of emitter objects, creation template:
            #bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=2, size=1.0, view_align=False, enter_editmode=False, location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0), layers=(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))¶
            ob = bpy.ops.mesh.primitive_uv_sphere_add(size=0.2, location=(float(xx),float(yy),float(zz)))
            
            emitter = bpy.context.object
 
            #bpy.ops.mesh.primitive_uv_sphere_add(size=0.2, location=(float(xx),float(yy),float(zz)))
            #psys1 = bpy.ops.mesh.primitive_uv_sphere_add(size=0.2, location=(float(xx),float(yy),float(zz)))
         
            bpy.ops.object.particle_system_add()    
            psys1 = emitter.particle_systems[-1]
                        
            psys1.name = 'Drops'
            
            # Emission    
            pset1 = psys1.settings
            pset1.name = 'DropSettings'
            pset1.normal_factor = 0.0
            pset1.object_factor = 1
            pset1.factor_random = 0
            pset1.frame_start = 0
            pset1.frame_end = bpy.context.scene.frame_current + 1
            pset1.lifetime = 500
            pset1.lifetime_random = 0
            pset1.emit_from = 'FACE'
            pset1.use_render_emitter = False
            pset1.object_align_factor = (0,0,0)
         
            pset1.count = bpy.data.scenes['Scene'].my_tool.int_box_n_particulas    #number of particles readed by the panel

            # Velocity
            pset1.normal_factor = 0.0
            pset1.factor_random = 0.0
         
            # Physics
            pset1.physics_type = 'NEWTON'
            pset1.mass = 0
            pset1.particle_size = 50
            pset1.use_multiply_size_mass = False
         
            #TODO ¿necessary?
            # Effector weights
            ew = pset1.effector_weights
            ew.gravity = 0
            ew.wind = 0
         
            # Children
            pset1.child_nbr = 0
            pset1.rendered_child_count = 0
            pset1.child_type = 'NONE'
         
            # Display and render
            pset1.draw_percentage = 100
            pset1.draw_method = 'CIRC'
            pset1.material = 1
            pset1.particle_size = 5   
            pset1.render_type = 'HALO'
            pset1.render_step = 0
                       
            nombreMesh = "Figura" + str(numeroObjeto)
            me = bpy.data.meshes.new(nombreMesh)
             


            #Blender saves the objects as "Sphere.xxx", where the "x" represents the number
            #It´s necesary a code to able the user to access objects every time the number changes

            if numeroObjeto==0:            
                nombreObjeto = "Sphere"
                ob = bpy.data.objects.get(nombreObjeto, me)
                ob.active_material = bluespecial
            if (numeroObjeto>0 and numeroObjeto<10):
                nombreObjeto = "Sphere.00" + str(numeroObjeto)
                ob = bpy.data.objects.get(nombreObjeto, me)
                if numeroObjeto==1:
                    ob.active_material = gold
                if numeroObjeto==2:
                    ob.active_material = turqoise
                if numeroObjeto==3:
                    ob.active_material = darkred
                if numeroObjeto==4:
                    ob.active_material = darkblue
            if (numeroObjeto>=10 and numeroObjeto<100):
                nombreObjeto = "Sphere.0" + str(numeroObjeto)
                ob = bpy.data.objects.get(nombreObjeto, me)
                ob.active_material = red
            
            scn = context.scene      # get the current scene
                 
                  
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)   #Refresh the actual visualization with the new generator object placed  
                  

            #Placing particles ------------------------
            #Reading the data to generate the function who originated it
            #Read the data from te panel 
            x_data_file = []
            y_data_file = []

            for line in file_with_data: 
                xData,yData, = line.split(" ")
                if xData == 'x' : #When all the data from one object is already readed it will be a line with 'x' to advert the system to stop reading
                    break 
                x_data_file.append(float(xData))
                y_data_file.append(float(yData))


            
            x_data_file, y_data_file = (list(t) for t in zip(*sorted(zip(x_data_file, y_data_file)))) #Order the two lists mantaining  their correlation
       
            funcInter = interpolate.interp1d(x_data_file, y_data_file) #Interpolation straight, creating the function to generate the points
             


                
            psys1 = ob.particle_systems[-1]  

            for pa in psys1.particles:
                #Añadiendo los datos del fichero a 3 variables 
                centroX = 0
                centroY = 0
                centroZ = 0

                while centroX == 0:
                    r1 = random.uniform(min(x_data_file),max(x_data_file));
                    r2 = random.uniform(min(x_data_file),max(x_data_file));
                    r3 = random.uniform(min(x_data_file),max(x_data_file));
                    if random.uniform(0, 1)*random.uniform(0, 1)*random.uniform(0, 1) <= funcInter(r1)*funcInter(r2)*funcInter(r3):
                        centroX = r1
                        centroY = r2
                        centroZ = r3
                    
                x_pos = (centroX + float(xx))
                y_pos = (centroY + float(yy))
                z_pos = (centroZ + float(zz))

                file_with_particles_data.write(str(x_pos) + " " + str(y_pos) + " " + str(z_pos) + "\n")
                
                pa.location = (x_pos,y_pos,z_pos) 

            numeroObjeto = numeroObjeto+1

            
        bpy.context.scene.frame_current = bpy.context.scene.frame_current + 1   #Goes one frame forward to show particles clear at rendering MANDATORY
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)     #Redraws to show particles new placement MANDATORY


        file_with_data.close()
        file_with_particles_data.close()


        return {'FINISHED'}            # this lets blender know the operator finished successfully.

        #me.from_pydata(verts, [], faces)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.

def register():
    bpy.utils.register_class(ParticlesCreationFromSpheres)


def unregister():
    bpy.utils.unregister_class(ParticlesCreationFromSpheres)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   
