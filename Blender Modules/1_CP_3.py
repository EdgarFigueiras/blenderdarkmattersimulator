bl_info = {    
    "name": "Particles Creation from Spheres",    
    "category": "Object",
}

import bpy
import time
import math
import random
import numpy as np
from scipy import interpolate
from scipy.interpolate import spline

class ParticlesCreationFromSpheres(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "particle.generator"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Creation from Spheres"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    #@staticmethod
    def execute(self,context):        # execute() is called by blender when running the operator.

        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)   #Refresh the actual visualization with the new generator object placed  

        #Reading the data to generate the function who originated it
        #Read the data from te panel 
        path = bpy.data.scenes['Scene'].my_tool.path #Origin from where the data will be readen, selected by the first option in the Panel
        
        file_with_binary_data = open(path, 'rb+') #File with binary data

        #Creating a file to save the particles placement information, helps to avoid a problem with Blender wich moves particles random
        file_with_particles_data = open(path + '.particles_info.txt', 'wb+')

        array_with_all_data = np.load(file_with_binary_data) #Gets the binary data as an array with 6 vectors (x_data, x_probability, y_data, y_probability, z_data, z_probability)


        #x_data_file, y_data_file = (list(t) for t in zip(*sorted(zip(x_data_file, y_data_file)))) #Order the two lists mantaining  their correlation
   
        funcInterX = interpolate.interp1d(array_with_all_data['arr_0'], array_with_all_data['arr_1']) #Interpolation straight, creating the function to generate the points
        funcInterY = interpolate.interp1d(array_with_all_data['arr_2'], array_with_all_data['arr_3']) #Interpolation straight, creating the function to generate the points
        funcInterZ = interpolate.interp1d(array_with_all_data['arr_4'], array_with_all_data['arr_5']) #Interpolation straight, creating the function to generate the points

        funcInterX_phase = interpolate.interp1d(array_with_all_data['arr_6'], array_with_all_data['arr_7']) #Interpolation straight, creating the function to generate the points of the phase
        funcInterY_phase = interpolate.interp1d(array_with_all_data['arr_8'], array_with_all_data['arr_9']) #Interpolation straight, creating the function to generate the points of the phase
        funcInterZ_phase = interpolate.interp1d(array_with_all_data['arr_10'], array_with_all_data['arr_11']) #Interpolation straight, creating the function to generate the points of the phase


        particles_number = bpy.data.scenes['Scene'].my_tool.int_box_n_particulas #Read from the panel 

        lista = [] #Array where all data will be stored

        for cont in range(particles_number):
            centroX = 0
            centroY = 0
            centroZ = 0
            phaseX = 0
            phaseY = 0
            phaseZ = 0

            while centroX == 0 :   
                xr1 = random.uniform(min(array_with_all_data['arr_0']),max(array_with_all_data['arr_0']));
                yr1 = random.uniform(min(array_with_all_data['arr_1']),max(array_with_all_data['arr_1']));
                if yr1 <= funcInterX(xr1):
                    centroX = xr1
                    if yr1 <= funcInterX_phase(xr1):
                        phaseX = funcInterX_phase(xr1)


            while centroY == 0 :   
                xr2 = random.uniform(min(array_with_all_data['arr_2']),max(array_with_all_data['arr_2']));
                yr2 = random.uniform(min(array_with_all_data['arr_3']),max(array_with_all_data['arr_3']));
                if yr2 <= funcInterY(xr2):
                    centroY = xr2
                    if yr2 <= funcInterY_phase(xr2):
                        phaseY = funcInterY_phase(xr2)

            while centroZ == 0 :   
                xr3 = random.uniform(min(array_with_all_data['arr_4']),max(array_with_all_data['arr_4']));
                yr3 = random.uniform(min(array_with_all_data['arr_5']),max(array_with_all_data['arr_5']));
                if yr3 <= funcInterZ(xr3):
                    centroZ = xr3
                    if yr3 <= funcInterZ_phase(xr3):
                        phaseZ = funcInterZ_phase(xr3)

                
            x_pos = (centroX)
            y_pos = (centroY)
            z_pos = (centroZ)

            phase = phaseX + phaseY + phaseZ

            if phase != 0 :
                phase = phase / 3
                phase = int(phase * 10)

            lista.append((x_pos, y_pos, z_pos, phaseX, phaseY, phaseZ, phase))

        lista.sort(key=lambda lista: lista[6], reverse=True)

        np.savez(file_with_particles_data,lista)
            
        bpy.context.scene.frame_current = bpy.context.scene.frame_current + 1   #Goes one frame forward to show particles clear at rendering MANDATORY
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)     #Redraws to show particles new placement MANDATORY

        file_with_binary_data.close()
        file_with_particles_data.close()

        bpy.ops.particle.colocation()

        return {'FINISHED'}            # this lets blender know the operator finished successfully.

        #me.from_pydata(verts, [], faces)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.

def register():
    bpy.utils.register_class(ParticlesCreationFromSpheres)


def unregister():
    bpy.utils.unregister_class(ParticlesCreationFromSpheres)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   
