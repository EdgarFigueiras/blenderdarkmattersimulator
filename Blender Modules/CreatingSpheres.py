bl_info = {    
    "name": "Planet rotation",    
    "category": "Object",
}

import bpy
import time
import math

class PlanetRotation(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.move_x"           # unique identifier for buttons and menu items to reference.
    bl_label = "Rotation with 2 objects"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

      
        f = open('/Users/edgarfigueiras/Documents/TFG DM/Blender Modules/datos.txt', 'r')
        # The original script
        scene = context.scene
        Sun = bpy.data.objects[2]
        #R = 32 # Numero total de movimientos 
        #for i in range(0, R, 1):
        for line in f:
            def sphere(scn, segments, rings, radius, x, y, z, r, g, b, a):
                xx,yy,zz = line.split(" ")
                me = Mesh.Primitives.Sphere(segments, rings, radius)
                
                mat = Material.New('SphereMat')
                mat.rgbCol = [r, g, b]
                mat.setAlpha(a)
                
                me.materials += [mat]
                
                ob = scn.objects.new(me, 'SphereOb')              
                ob.setLocation(float(xx), float(yy), float(zz))
            
            sce = bpy.data.scenes.active

            sphere(sce, 32, 32, 4, 1, 1, 1, .2, .5, .6, 1.0)

            time.sleep(0.01)
            
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1) #Actualiza el estado gráfico para apreciar la animacion

        return {'FINISHED'}            # this lets blender know the operator finished successfully.
        f.close()

def register():
    bpy.utils.register_class(PlanetRotation)


def unregister():
    bpy.utils.unregister_class(PlanetRotation)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   