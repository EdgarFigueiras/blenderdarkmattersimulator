import bpy

bl_info = {    
    "name": "Camera Movement2",    
    "category": "Object",
}

import bpy
import time
import math
import random
import numpy as np
from scipy import interpolate
from scipy.interpolate import spline

class CameraMovement2(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.camera2"           # unique identifier for buttons and menu items to reference.
    bl_label = "Camera Movement2"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.
            
            
        
        if(len(bpy.data.cameras) == 1):
            
            camera_1 = bpy.data.objects['Camera'] # bpy.types.Camera

          
            R = 32 # Numero total de movimientos 
            longitud_arco = 1.597
            sentido = -1 # 1 Derecha, -1 Izquierda
            

            for i in range(0, R, 1): 
                x2 = camera_1.location.x
                y2 = camera_1.location.y
             
                
                x1 = 0
                y1 = 0
                
                radio = math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
                
                angulo = longitud_arco / radio
                
                angulo = angulo * sentido

                camera_1.location.x = x1 + ( x2 - x1 ) * math.cos(angulo) - ( y2 - y1 ) * math.sin(angulo)
                camera_1.location.y = y1 + ( x2 - x1 ) * math.sin(angulo) + ( y2 - y1 ) * math.cos(angulo)
                
                bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1) #Actualiza el estado gráfico para apreciar la animacion

                #bpy.context.scene.render.filepath = '/Users/edgarfigueiras/Desktop/Test/vr_shot_%d.jpg' % i
                #bpy.ops.render.render( write_still=True )

                #Avanzar en la escena para mostrar los cambios al renderizar OBLIGATORIO
                bpy.context.scene.frame_current = bpy.context.scene.frame_current + 1

        return {'FINISHED'}            # this lets blender know the operator finished successfully.
             
    
    

      


def register():
    bpy.utils.register_class(CameraMovement2)


def unregister():
    bpy.utils.unregister_class(CameraMovement2)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   
