import bpy

bl_info = {    
    "name": "Camera Movement",    
    "category": "Object",
}

import bpy


class CameraMovement(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.camera"           # unique identifier for buttons and menu items to reference.
    bl_label = "Camera Movement"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

        if(len(bpy.data.cameras) == 1):
            obj = bpy.data.objects['Camera'] # bpy.types.Camera
            obj.location.x = 0.0
            obj.location.y = -10.0
            obj.location.z = 10.0
            obj.keyframe_insert(data_path="location", frame=10.0)
      
       #Avanzar en la escena para mostrar los cambios al renderizar OBLIGATORIO
        bpy.context.scene.frame_current = bpy.context.scene.frame_current + 1
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
             
    
    
def register():
    bpy.utils.register_class(CameraMovement)


def unregister():
    bpy.utils.unregister_class(CameraMovement)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   