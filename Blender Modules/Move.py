from bge import logic, events

cont = logic.getCurrentController()
keyboard = logic.keyboard.events

x, y, z = 0, 0, 0

if keyboard[events.UPARROWKEY]: x += 3
if keyboard[events.DOWNARROWKEY]: x -= 3
if keyboard[events.LEFTARROWKEY]: y += 3
if keyboard[events.RIGHTARROWKEY]: y -= 3
if keyboard[events.QKEY]: z += 1
if keyboard[events.AKEY]: z -= 1

cont.owner.applyTorque((x, y, 0))
cont.owner.applyMovement((x*0.1,y*0.1,z*0.1))
#pcont.owner.applyForce((x, y, z))