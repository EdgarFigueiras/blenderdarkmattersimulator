bl_info = {    
    "name": "Particles Colocation",    
    "category": "Object",
}

import bpy
import time
import math
import random
import struct
import binascii
import numpy as np

class ParticlesColocation(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "particle.colocation"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Colocation"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    #@staticmethod
    def execute(self,context):        # execute() is called by blender when running the operator.



        #Hacer que esta variable se lea por el panel
        file_with_particles_data = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/ParticlesFreezed.txt', 'r') 
        numeroObjetos = file_with_particles_data.readline()

        x = 0

        for x in range(0,int(numeroObjetos)):
            if x == 0 : 
                emitter = bpy.data.objects['Sphere']
            if (x > 0 and x < 10) :
                emitter = bpy.data.objects['Sphere.00' + str(x)]
            if (x > 10) :
                emitter = bpy.data.objects['Sphere.0' + str(x)]

                
            psys1 = emitter.particle_systems[-1]     

            for pa in psys1.particles:

                line = file_with_particles_data.readline()
                x_pos,y_pos,z_pos = line.split(" ") 
                               
                xx = (float(x_pos))
                yy = (float(y_pos))
                zz = (float(z_pos))

                pa.location = (xx,yy,zz)

            x = x+1


        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        #Avanzar en la escena para mostrar los cambios al renderizar OBLIGATORIO
        bpy.context.scene.frame_current = bpy.context.scene.frame_current + 1

        file_with_particles_data.close()

        return {'FINISHED'}            # this lets blender know the operator finished successfully.

        #me.from_pydata(verts, [], faces)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.

def register():
    bpy.utils.register_class(ParticlesColocation)


def unregister():
    bpy.utils.unregister_class(ParticlesColocation)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   
