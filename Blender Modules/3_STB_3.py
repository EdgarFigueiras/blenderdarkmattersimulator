bl_info = {    
    "name": "Particles Stabilizer",    
    "category": "Object",
}

import bpy
import time
import math
import random
import struct
import binascii
import numpy as np

class ParticlesStabilizer(bpy.types.Operator):
    """My Object Moving Script"""               # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "particle.stabilizer"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Stabilization"        # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}           # enable undo for the operator.
   
    #@staticmethod
    def execute(self,context):        # execute() is called by blender when running the operator.


        #File where the particle data was stored
        path = bpy.data.scenes['Scene'].my_tool.path
        file_with_particles_data = open(path + '.particles_info_extra.txt', 'r+')

        line = file_with_particles_data.readline()
        numeroObjetos = line[0]         #Stores the number of particle generators to iterate over it for place it

        numeroObjetos = int(numeroObjetos) + 1

        x = 0

        for x in range(0,int(numeroObjetos)):
            if x == 0 : 
                emitter = bpy.data.objects['Sphere']
            if (x > 0 and x < 10) :
                emitter = bpy.data.objects['Sphere.00' + str(x)]

                
            psys1 = emitter.particle_systems[-1]     

            for pa in psys1.particles:

                line = file_with_particles_data.readline()
                x_pos,y_pos,z_pos,residual = line.split(" ") 
                               
                xx = (float(x_pos))
                yy = (float(y_pos))
                zz = (float(z_pos))

                if pa.die_time < 500 :
                    pa.die_time = 500
                    pa.lifetime = 500
                    pa.velocity = (0,0,0)

                pa.location = (xx,yy,zz)

            x = x+1


        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        #Make the scene go one frame forward MANDATORY
        bpy.context.scene.frame_current = bpy.context.scene.frame_current + 1

        file_with_particles_data.close()


        return {'FINISHED'}            # this lets blender know the operator finished successfully.

# ------------------------------------------------------------------------
#    Register and unregister functions
# ------------------------------------------------------------------------

def register():
    bpy.utils.register_class(ParticlesStabilizer)


def unregister():
    bpy.utils.unregister_class(ParticlesStabilizer)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   
