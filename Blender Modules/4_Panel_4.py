import bpy
import time
from bpy.props import (StringProperty,
                        IntProperty,
                       PointerProperty,
                       )
from bpy.types import (Panel,
                       Operator,
                       AddonPreferences,
                       PropertyGroup,
                       )

# ------------------------------------------------------------------------
#    Panel which allows the user to interact with the simulator
# ------------------------------------------------------------------------

#Clean the scene
bpy.ops.object.select_by_type(type='MESH')
bpy.ops.object.delete()

class MySettings(PropertyGroup):

    path = StringProperty(
        name="Data File",
        description="Select the file with the simulation data.",
        default="",
        maxlen=1024,
        subtype='FILE_PATH')

    image_path = StringProperty(
        name="Store Path",
        description="Path where renders will be stored, by default uses the path of the simulation data",
        default="",
        maxlen=1024,
        subtype='DIR_PATH')

    int_box_n_particulas = IntProperty(
        name="Particles N ", 
        description="Total number of particles of the simulation",
        min = 50, max = 500,
        default = 150)

    int_box_granularity = IntProperty(
        name="Granularity ", 
        description="Modifies the granularity. Min = 1 , Max = 10",
        min = 1, max = 10,
        default = 5)

    int_box_saturation = IntProperty(
        name="Saturation ", 
        description="Modify the saturation. Min = 1, Max = 10",
        min = 1, max = 10,
        default = 5)
 
class OBJECT_OT_ResetButton(bpy.types.Operator):
    bl_idname = "reset.image"
    bl_label = "Reiniciar entorno"
    country = bpy.props.StringProperty()

    def execute(self, context):
        bpy.context.space_data.viewport_shade = 'MATERIAL'
        bpy.ops.object.select_by_type(type='MESH')
        bpy.ops.object.delete()
        bpy.context.scene.frame_current = 0
        return{'FINISHED'} 

class OBJECT_OT_RenderButton(bpy.types.Operator):
    bl_idname = "render.image"
    bl_label = "Renderizar"
    country = bpy.props.StringProperty()

    #This code 
    def execute(self, context):

        dir_image_path = bpy.data.scenes['Scene'].my_tool.image_path

        if dir_image_path == "":
            bpy.data.scenes['Scene'].render.filepath = bpy.data.scenes['Scene'].my_tool.path + time.strftime("%c%s") + '.jpg'
        else:
            bpy.data.scenes['Scene'].render.filepath = dir_image_path + time.strftime("%c%s") + '.jpg'
     
        bpy.ops.render.render( write_still=True ) 
        return{'FINISHED'} 

      


class OBJECT_PT_my_panel(Panel):
    bl_idname = "OBJECT_PT_my_panel"
    bl_label = "Simulation Panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "Tools"
    bl_context = "objectmode"

class Panel(bpy.types.Panel):
    """Panel para añadir al entorno 3D"""
    bl_label = "Simulation Panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'


    def draw(self, context):
        layout = self.layout
        scn = context.scene
        col = layout.column(align=True)
        box1 = layout.box()
        box2 = layout.box()
        box3 = layout.box()
        row = layout.row()

        box1.label(text="PARAMETERS")

        box1.label(text="Select the data file", icon='LIBRARY_DATA_DIRECT')

        box1.prop(scn.my_tool, "path", text="")

        box1.label(text="Select the number of particles", icon='PARTICLE_DATA')

        box1.prop(scn.my_tool, "int_box_n_particulas")

        box1.label(text="Select the granularity", icon='GROUP_VERTEX')

        box1.prop(scn.my_tool, "int_box_granularity")

        box1.label(text="Select the saturation", icon='GROUP_VCOL')

        box1.prop(scn.my_tool, "int_box_saturation")

        box2.label(text="SIMULATION")

        box2.operator("particle.generator", text="Run Simulation")

        box2.operator("particle.stabilizer", text="Place Particles")

        col.label(text="To switch view press SHIFT + Z", icon='INFO')

        box3.label(text="RENDER")

        box3.label(text="Select the folder to store renders")

        box3.prop(scn.my_tool, "image_path", text="")

        box3.operator("render.image", text="Render")

        box3.operator("reset.image", text="Reset Environment")


# ------------------------------------------------------------------------
#    Register and unregister functions
# ------------------------------------------------------------------------

def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.my_tool = PointerProperty(type=MySettings)

def unregister():
    bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.my_tool

if __name__ == "__main__":
    register()
