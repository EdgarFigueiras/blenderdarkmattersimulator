bl_info = {    
    "name": "Particles Colocation",    
    "category": "Object",
}

import bpy
import time
import math
import random
import numpy as np

class ParticlesColocation(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.move_x"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Colocation"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

    #Funcion que genera numeros basandose en la distribución de la campana de Gauss
    # mu=>centro. sigma=>desviacion
        def gauss_distribution(mu, sigma):
            x = random.gauss(mu, sigma)
            return x

        numero_objetos = 4
                       
        #f = open('/Users/edgarfigueiras/Documents/TFG DM/Blender Modules/datos_particles.txt', 'r')
        #for line in f: 
        #for x in range(N):
        	#r = random.uniform(-10,10);
	        #if random.uniform(0, 1) < gaussV2(r):
		       #array_resultados.append(r)
        for x in range(0,numero_objetos):
            emitter = bpy.data.objects[x+2]
            psys1 = emitter.particle_systems[-1]                 
            for pa in psys1.particles:
                #Añadiendo los datos del fichero a 3 variables 
                if x == 0 : 
                    centro = random.uniform(-2,0);
                if x == 1 : 
                    centro = random.uniform(-1,1);
                if x == 2 : 
                    centro = random.uniform(0,3);
                if x == 3 : 
                    centro = random.uniform(4,6);
                    
                gauss_x = gauss_distribution(centro,1)
                gauss_y = gauss_distribution(centro,1)
                gauss_z = gauss_distribution(centro,1)
                
                #xx,yy,zz = line.split(" ")  
                pa.location = (gauss_x,gauss_y,gauss_z)   
                #Actualiza el estado gráfico para apreciar la animacion          
                bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)  
        #me.from_pydata(verts, [], faces)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
        f.close()

def register():
    bpy.utils.register_class(ParticlesColocation)


def unregister():
    bpy.utils.unregister_class(ParticlesColocation)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   