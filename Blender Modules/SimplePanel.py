import bpy

class Panel(bpy.types.Panel):
    """Panel para añadir al entorno 3D"""
    bl_label = "New Module"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    
    def draw(self,content):
        layout = self.layout
        
        row = layout.row()
        row.label(text="Choose an option:")
        
        split = layout.split()
        col = split.column(align=True)
        
        col.operator("object.creation", text="ParticlesCreation")
        col.operator("object.colocation", text="ParticlesColocation")
        col.operator("object.camera2", text="CameraRotation")
  
        
def register():
    bpy.utils.register_class(Panel) 
def unregister():
    bpy.utils.unregister_class(Panel)
    
if __name__ == "__main__":
    register()