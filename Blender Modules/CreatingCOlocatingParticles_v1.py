bl_info = {    
    "name": "Particles Creation from Spheres",    
    "category": "Object",
}

import bpy
import time
import math
import random
import numpy as np
from scipy import interpolate
from scipy.interpolate import spline

class ParticlesCreationFromSpherer(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.creation"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Creation from Spheres"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

        #Borrar todos los objetos de la escena
        bpy.ops.object.select_by_type(type='MESH')
        bpy.ops.object.delete()
      
        #Definir un material
        def makeMaterial(name, diffuse, specular, alpha):
            mat = bpy.data.materials.new(name)
            mat.diffuse_color = diffuse
            mat.type = 'HALO'
            #Modifica el tamaño del halo de las particulas IMPORTANTE
            mat.halo.size = 2
            mat.diffuse_intensity = 10.0
            mat.specular_color = specular
            mat.specular_intensity = 0.5
            mat.alpha = alpha
            mat.ambient = 1
            return mat
        
        
        #Creo materiales y los nombro en funcion a su color
        red = makeMaterial('Red',(1,0,0),(1,1,1),1)
        green = makeMaterial('Green',(0,1,0),(1,1,1),1)
        blue = makeMaterial('Blue',(0,0,1),(1,1,1),1)
        white = makeMaterial('White',(1,1,1),(1,1,1),1)
    
        #PARTE DE CREACION
        numeroObjeto = 000
        #Origen de donde leo los datos, usado para crear las bases de las cuales se originan las particulas
        f = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/datosV3.txt', 'r')
        for line in f:
             
            #Añadiendo los datos del fichero a 3 variables 
            xx,yy,zz = line.split(" ")
            
            #Creando el objeto, plantilla:
            #bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=2, size=1.0, view_align=False, enter_editmode=False, location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0), layers=(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))¶
            ob = bpy.ops.mesh.primitive_uv_sphere_add(size=0.2, location=(float(xx),float(yy),float(zz)))
            
            emitter = bpy.context.object
 
            #bpy.ops.mesh.primitive_uv_sphere_add(size=0.2, location=(float(xx),float(yy),float(zz)))
            #psys1 = bpy.ops.mesh.primitive_uv_sphere_add(size=0.2, location=(float(xx),float(yy),float(zz)))
         
            bpy.ops.object.particle_system_add()    
            psys1 = emitter.particle_systems[-1]
                        
            psys1.name = 'Drops'
            
            # Emission    
            pset1 = psys1.settings
            pset1.name = 'DropSettings'
            pset1.normal_factor = 0.0
            pset1.object_factor = 1
            pset1.factor_random = 0
            pset1.frame_start = 0
            pset1.frame_end = 1
            pset1.lifetime = 400
            pset1.lifetime_random = 0
            pset1.emit_from = 'FACE'
            pset1.use_render_emitter = False
            pset1.object_align_factor = (0,0,0)
            #Importante numero de particulas por cada generador origen
            pset1.count = 10000
         
             # Velocity
            pset1.normal_factor = 0.0
            pset1.factor_random = 0.0
         
          

            # Physics
            pset1.physics_type = 'NEWTON'
            pset1.mass = 0
            pset1.particle_size = 50
            pset1.use_multiply_size_mass = False
         
            # Effector weights
            ew = pset1.effector_weights
            ew.gravity = 0
            ew.wind = 0
         
            # Children
            pset1.child_nbr = 0
            pset1.rendered_child_count = 0
            pset1.child_type = 'NONE'
         
            # Display and render
            pset1.draw_percentage = 100
            pset1.draw_method = 'CIRC'
            pset1.material = 1
            pset1.particle_size = 5   
            pset1.render_type = 'HALO'
            pset1.render_step = 3
                       
            nombreMesh = "Figura" + str(numeroObjeto)
            me = bpy.data.meshes.new(nombreMesh)
             

            #Debido a que los objetos son nombrados "Sphere.xxx" Donde las x representa el numero
            #Es necesario un codigo para poder acceder a los objetos cada vez que la cifra cambia de
            #digitos. 
            if numeroObjeto==0:            
                nombreObjeto = "Sphere"
                ob = bpy.data.objects.get(nombreObjeto, me)
                ob.active_material = red
            if (numeroObjeto>0 and numeroObjeto<10):
                nombreObjeto = "Sphere.00" + str(numeroObjeto)
                ob = bpy.data.objects.get(nombreObjeto, me)
                ob.active_material = blue
                if numeroObjeto==1:
                    ob.active_material = white
                if numeroObjeto==2:
                    ob.active_material = white
            if (numeroObjeto>=10 and numeroObjeto<100):
                nombreObjeto = "Sphere.0" + str(numeroObjeto)
                ob = bpy.data.objects.get(nombreObjeto, me)
                ob.active_material = white
            
            scn = context.scene      # get the current scene
                 
            
                
            #Actualiza el estado gráfico para apreciar la animacion          
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1) 

            numeroObjeto += 1
        
        f.close()    

        #bpy.ops.screen.animation_play(reverse=False, sync=False)
        bpy.context.scene.frame_current = 1  

        #PARTE DE COLOCACION
        #Reading the data to generate the function who originated it
        file_with_data = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/dataToInterpolate4.txt', 'r')
        x_data_file = []
        y_data_file = []

        for line in file_with_data: 
            xData,yData = line.split(" ") 
            x_data_file.append(float(xData))
            y_data_file.append(float(yData))

        #Order the two lists mantaining  their correlation
        x_data_file, y_data_file = (list(t) for t in zip(*sorted(zip(x_data_file, y_data_file))))

        #Interpolation straight, creating the function to generate the points
        funcInter = interpolate.interp1d(x_data_file, y_data_file)


        file_with_data2 = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/dataToInterpolate2.txt', 'r')
        x_data_file2 = []
        y_data_file2 = []

        for line in file_with_data2: 
            xData2,yData2 = line.split(" ") 
            x_data_file2.append(float(xData2))
            y_data_file2.append(float(yData2))

        #Order the two lists mantaining  their correlation
        x_data_file2, y_data_file2 = (list(t) for t in zip(*sorted(zip(x_data_file2, y_data_file2))))

        #Interpolation straight, creating the function to generate the points
        funcInter2 = interpolate.interp1d(x_data_file2, y_data_file2)

        #Reading the data to generate the function who originated it
        file_with_data3 = open('/Users/edgarfigueiras/Documents/TFG DM/blenderdarkmattersimulator/Blender Modules/gaussSimple.txt', 'r')
        x_data_file3 = []
        y_data_file3 = []

        for line in file_with_data3: 
            xData3,yData3 = line.split(" ") 
            x_data_file3.append(float(xData3))
            y_data_file3.append(float(yData3))

        #Order the two lists mantaining  their correlation
        x_data_file3, y_data_file3 = (list(t) for t in zip(*sorted(zip(x_data_file3, y_data_file3))))

        #Interpolation straight, creating the function to generate the points
        funcInter3 = interpolate.interp1d(x_data_file3, y_data_file3)
         

        for x in range(0,numeroObjeto):
            if x == 0 : 
                emitter = bpy.data.objects['Sphere']
            if x == 1 :
                emitter = bpy.data.objects['Sphere.001']

                
            psys1 = emitter.particle_systems[-1]                 
            for pa in psys1.particles:
                #Añadiendo los datos del fichero a 3 variables 
                centroX = 0
                centroY = 0
                centroZ = 0
                if x == 0 : 
                    while centroX == 0:
                        r = random.uniform(min(x_data_file3),max(x_data_file3));
                        if random.uniform(0, 1) < funcInter3(r):
                            centroX = r
                    while centroY == 0:
                        r = random.uniform(min(x_data_file3),max(x_data_file3));
                        if random.uniform(0, 1) < funcInter3(r):
                            centroY = r
                    while centroZ == 0:
                        r = random.uniform(min(x_data_file3),max(x_data_file3));
                        if random.uniform(0, 1) < funcInter3(r):
                            centroZ = r
                if x == 1 : 
                    while centroX == 0:
                        r = random.uniform(min(x_data_file),max(x_data_file));
                        if random.uniform(0, 1) < funcInter(r):
                            centroX = r
                    while centroY == 0:
                        r = random.uniform(min(x_data_file),max(x_data_file));
                        if random.uniform(0, 1) < funcInter(r):
                            centroY = r
                    while centroZ == 0:
                        r = random.uniform(min(x_data_file),max(x_data_file));
                        if random.uniform(0, 1) < funcInter(r):
                            centroZ = r
                    
                x_pos = (centroX)
                y_pos = (centroY)
                z_pos = (centroZ)
                
                pa.location = (x_pos,y_pos,z_pos)   
                #Refresh the graphics to show the changes at realtime          
                #bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)  

        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        #Avanzar en la escena para mostrar los cambios al renderizar OBLIGATORIO
        bpy.context.scene.frame_current = bpy.context.scene.frame_current + 1
        file_with_data.close()
        file_with_data2.close()
        return {'FINISHED'}            # this lets blender know the operator finished successfully.

        #me.from_pydata(verts, [], faces)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.

def register():
    bpy.utils.register_class(ParticlesCreationFromSpherer)


def unregister():
    bpy.utils.unregister_class(ParticlesCreationFromSpherer)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   
