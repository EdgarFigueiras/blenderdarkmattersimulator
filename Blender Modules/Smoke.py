#----------------------------------------------------------
# File smoke.py
# Creates smoke and smoke material. 
# Heavily inspired by Andrew Price's tutorial at
# http://www.blenderguru.com/introduction-to-smoke-simulation/
#----------------------------------------------------------
import bpy, mathutils, math
from mathutils import Vector
from math import pi
 
def createDomain(origin):
    # Add cube as domain
    bpy.ops.mesh.primitive_cube_add(location=origin)
    bpy.ops.transform.resize(value=(4, 4, 4))
    domain = bpy.context.object
    domain.name = 'Domain'
 
    # Add domain modifier
    dmod = domain.modifiers.new(name='Smoke', type='SMOKE')
    dmod.smoke_type = 'DOMAIN'
    dset = dmod.domain_settings
 
    # Domain settings
    dset.resolution_max = 32
    dset.alpha = -0.001
    dset.beta = 2.0
    dset.time_scale = 1.2
    dset.vorticity = 2.0
    dset.use_dissolve_smoke = True
    dset.dissolve_speed = 80
    dset.use_dissolve_smoke_log = True
    dset.use_high_resolution = True
    dset.show_high_resolution = True
 
    # Effector weights
    ew = dset.effector_weights
    ew.gravity = 0.4
    ew.force = 0.8
    return domain
 
def createFlow(origin):
    # Add plane as flow
    bpy.ops.mesh.primitive_plane_add(location = origin)
    bpy.ops.transform.resize(value=(2, 2, 2))
    flow = bpy.context.object
    flow.name = 'Flow'
 
    # Add smoke particle system
    pmod = flow.modifiers.new(name='SmokeParticles', type='PARTICLE_SYSTEM')
    pmod.name = 'SmokeParticles'
    psys = pmod.particle_system
    psys.seed = 4711
 
    # Particle settings
    pset = psys.settings
    pset.type = 'EMITTER'
    pset.lifetime = 1
    pset.emit_from = 'VOLUME'
    pset.use_render_emitter = False
    pset.render_type = 'NONE'
    pset.normal_factor = 8.0
 
    # Add smoke modifier
    smod = flow.modifiers.new(name='Smoke', type='SMOKE')
    smod.smoke_type = 'FLOW'
    sfset = smod.flow_settings
 
    # Flow settings
    sfset.use_outflow = False
    sfset.temperature = 0.7
    sfset.density = 0.8
    sfset.initial_velocity = True
    sfset.particle_system = psys
    return flow
 
def createVortexEffector(origin):
    bpy.ops.object.effector_add(type='VORTEX', location=origin)
    vortex = bpy.context.object
    return vortex
 
def createVoxelTexture(domain):
    tex = bpy.data.textures.new('VoxelTex', type = 'VOXEL_DATA')
    voxdata = tex.voxel_data
    voxdata.file_format = 'SMOKE'
    voxdata.domain_object = domain
    return tex
 
def createVolumeMaterial(tex):    
    mat = bpy.data.materials.new('VolumeMat')
    mat.type = 'VOLUME'
    vol = mat.volume
    vol.density = 0.0
    vol.density_scale = 8.0
    vol.scattering = 6.0
    vol.asymmetry = 0.3
    vol.emission = 0.3
    vol.emission_color = (1,1,1)
    vol.transmission_color = (0.9,0.2,0)
    vol.reflection = 0.7
    vol.reflection_color = (0.8,0.9,0)
    # To remove pixelation effects
    vol.step_size = 0.05
 
    # Add voxdata texture
    mtex = mat.texture_slots.add()
    mtex.texture = tex
    mtex.texture_coords = 'ORCO'
    mtex.use_map_density = True
    mtex.use_map_emission = True
    mtex.use_map_scatter = False
    mtex.use_map_reflect = True
    mtex.use_map_color_emission = True
    mtex.use_map_color_transmission = True
    mtex.use_map_color_reflection = True
 
    mtex.density_factor = 1.0
    mtex.emission_factor = 0.2
    mtex.scattering_factor = 0.2
    mtex.reflection_factor = 0.3    
    mtex.emission_color_factor = 0.9
    mtex.transmission_color_factor = 0.5
    mtex.reflection_color_factor = 0.6    
    return mat
 
def addFloor(origin):
    # Create floor that receives transparent shadows
    bpy.ops.mesh.primitive_plane_add(
        location = origin,
        rotation = (0, 0, pi/4))
    bpy.ops.transform.resize(value=(4, 4, 4))
    bpy.ops.transform.resize(value=(2, 2, 2), 
        constraint_axis=(True, False, False), 
        constraint_orientation='LOCAL')
    floor = bpy.context.object
    mat = bpy.data.materials.new('Floor')
    mat.use_transparent_shadows = True
    floor.data.materials.append(mat)
    return
 
def setupWorld():
    scn = bpy.context.scene
    # Blue blend sky
    scn.world.use_sky_blend = True
    scn.world.horizon_color = (0.25, 0.3, 0.4)
    scn.world.zenith_color = (0, 0, 0.7)
    # PAL 4:3 render
    scn.render.resolution_x = 720
    scn.render.resolution_y = 567    
    return
 
def run(origin):
    domain = createDomain(origin)
    flow = createFlow(origin-Vector((0,0,3.5)))
    vortex = createVortexEffector(origin)
    tex = createVoxelTexture(domain)
    mat = createVolumeMaterial(tex)
    domain.data.materials.append(mat)
    return
 
if __name__ == "__main__":
    for ob in bpy.context.scene.objects:
        bpy.context.scene.objects.unlink(ob)
 
    addFloor(Vector((0,0,-4)))
    setupWorld()
    # Lights and camera
    bpy.ops.object.lamp_add( type = 'POINT', location=(4,6,1))
    bpy.ops.object.lamp_add( type = 'POINT', location=(-7,-5,0))
    bpy.ops.object.camera_add(location=Vector((8,-8,3)),
        rotation=(pi/3, 0, pi/6))
 
    run(Vector((0,0,0)))
    bpy.ops.screen.animation_play()