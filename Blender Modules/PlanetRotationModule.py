bl_info = {    
    "name": "Planet rotation",    
    "category": "Object",
}

import bpy
import time
import math

class PlanetRotation(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.move_x"           # unique identifier for buttons and menu items to reference.
    bl_label = "Rotation with 2 objects"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

        # The original script
        scene = context.scene
        R = 32 # Numero total de movimientos 
        longitud_arco = 1.597
        sentido = -1 # 1 Derecha, -1 Izquierda
        for i in range(0, R, 1):
            x2 = bpy.data.objects[3].location.x
            y2 = bpy.data.objects[3].location.y
            
            x1 = bpy.data.objects[2].location.x
            y1 = bpy.data.objects[2].location.y
            
            radio = math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
            
            angulo = longitud_arco / radio
            
            angulo = angulo * sentido

            bpy.data.objects[3].location.x = x1 + ( x2 - x1 ) * math.cos(angulo) - ( y2 - y1 ) * math.sin(angulo)
            bpy.data.objects[3].location.y = y1 + ( x2 - x1 ) * math.sin(angulo) + ( y2 - y1 ) * math.cos(angulo)
            
            time.sleep(0.01)
            
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1) #Actualiza el estado gráfico para apreciar la animacion

        return {'FINISHED'}            # this lets blender know the operator finished successfully.

def register():
    bpy.utils.register_class(PlanetRotation)


def unregister():
    bpy.utils.unregister_class(PlanetRotation)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   