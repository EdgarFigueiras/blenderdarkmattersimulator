bl_info = {    
    "name": "Particles Colocation",    
    "category": "Object",
}

import bpy
import time
import math

class ParticlesColocation(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.move_x"           # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Colocation"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

       
        emitter = bpy.context.object  
        psys1 = emitter.particle_systems[-1]
             
        f = open('/Users/edgarfigueiras/Documents/TFG DM/Blender Modules/datos_particles.txt', 'r')
        for line in f:                  
            for (line,pa) in zip(f,psys1.particles):
                #Añadiendo los datos del fichero a 3 variables 
                xx,yy,zz = line.split(" ")  
                pa.location = (float(xx),float(yy),float(zz))   
                #Actualiza el estado gráfico para apreciar la animacion          
                bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)  
                            
            
         

        #me.from_pydata(verts, [], faces)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
        f.close()

def register():
    bpy.utils.register_class(ParticlesColocation)


def unregister():
    bpy.utils.unregister_class(ParticlesColocation)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   