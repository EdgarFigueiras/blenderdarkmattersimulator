bl_info = {    
    "name": "Particles Creation from Spheres",    
    "category": "Object",
}

import bpy
import time
import math
import random
import numpy as np
from scipy import interpolate
from scipy.interpolate import spline

class ParticlesCreationFromSpheres(bpy.types.Operator):
    """My Object Moving Script"""                 # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "particle.generator"              # unique identifier for buttons and menu items to reference.
    bl_label = "Particles Creation from Spheres"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}             # enable undo for the operator.
   
    #@staticmethod
    def execute(self,context):        # execute() is called by blender when running the operator.

        def calculatePosition(array_1, array_2, function):
            position = 0
            amplittude = 0
            while position == 0 :
                x = random.uniform(min(array_1),max(array_1));
                y = random.uniform(min(array_2),max(array_2));
                if y <= function(x):
                    position = x
                    amplittude = function(x)

            return position, amplittude

       
        #To calculate the amplittude of the particle is made the average of the 3 datas from the diferent axis
        def calculateAmplittude (p_x, p_y, p_z): 
            ampl = p_x + p_y + p_z
            ampl = ampl / 3
            ampl = int(ampl * 10)   #We only take the first digit, by this way the system to create the particles with diferent colour becomes normalized 

            return ampl

        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)   #Refresh the actual visualization with the new generator object placed  

        #Reading the data to generate the function who originated it
        #Read the data from te panel 
        path = bpy.data.scenes['Scene'].my_tool.path #Origin from where the data will be readen, selected by the first option in the Panel
        
        file_with_binary_data = open(path, 'rb+') #File with binary data

        #Creating a file to save the particles placement information, helps to avoid a problem with Blender wich moves particles random
        file_with_particles_data = open(path + '.particles_info.txt', 'wb+')

        array_with_all_data = np.load(file_with_binary_data) #Gets the binary data as an array with 6 vectors (x_data, x_probability, y_data, y_probability, z_data, z_probability)
   
        funcInterX = interpolate.interp1d(array_with_all_data['arr_0'], array_with_all_data['arr_1']) #Interpolation straight, creating the function to generate the points
        funcInterY = interpolate.interp1d(array_with_all_data['arr_2'], array_with_all_data['arr_3']) #Interpolation straight, creating the function to generate the points
        funcInterZ = interpolate.interp1d(array_with_all_data['arr_4'], array_with_all_data['arr_5']) #Interpolation straight, creating the function to generate the points

        particles_number = bpy.data.scenes['Scene'].my_tool.int_box_n_particulas #Read from the panel 

        lista = [] #Array where all data will be stored

        for cont in range(particles_number):
            x_pos, amplX = calculatePosition(array_with_all_data['arr_0'],array_with_all_data['arr_1'],funcInterX)
            y_pos, amplY = calculatePosition(array_with_all_data['arr_2'],array_with_all_data['arr_3'],funcInterY)
            z_pos, amplZ = calculatePosition(array_with_all_data['arr_4'],array_with_all_data['arr_5'],funcInterZ)

            ampl = calculateAmplittude(amplX, amplY, amplZ)

            lista.append((x_pos, y_pos, z_pos, amplX, amplY, amplZ, ampl))    #All the data is stored together

        #Top down sorting using amplittude before calculated as the element to sort 
        lista.sort(key=lambda lista: lista[6], reverse=True)    #Sort the list placing the data with bigger amplittude at the top

        np.savez(file_with_particles_data,lista)
            
        bpy.context.scene.frame_current = bpy.context.scene.frame_current + 1   #Goes one frame forward to show particles clear at rendering MANDATORY
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)             #Redraws to show particles new placement MANDATORY

        file_with_binary_data.close()
        file_with_particles_data.close()

        bpy.ops.particle.colocation()

        return {'FINISHED'}            # this lets blender know the operator finished successfully.

# ------------------------------------------------------------------------
#    Register and unregister functions
# ------------------------------------------------------------------------

def register():
    bpy.utils.register_class(ParticlesCreationFromSpheres)


def unregister():
    bpy.utils.unregister_class(ParticlesCreationFromSpheres)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   
