import bpy
#creates a mesh object
#from the selected particle system
#from the current frame

obj=bpy.context.active_object

ps=obj.particle_systems[0]

count=0
for pl in ps.particles:
    if pl.alive_state=='ALIVE':
        count+=1

mesh=bpy.data.meshes.new('point_cloud')
mesh.vertices.add(count)

for i in range(count):
    mesh.vertices[i].co=ps.particles[i].location

dupli=bpy.data.objects.new('point_cloud',mesh)
bpy.context.scene.objects.link(dupli)