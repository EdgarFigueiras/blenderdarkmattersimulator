bl_info = {    
    "name": "Planet rotation 3",    
    "category": "Object",
}

import bpy
import time
import math

class PlanetRotation3(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.move_x_3"           # unique identifier for buttons and menu items to reference.
    bl_label = "Rotation with 3 objects"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

        # The original script
        scene = context.scene
        R = 64 # Numero total de movimientos 
        longitud_arco = 1.597
        longitud_arco_Mars = 1.597
        longitud_arco_Comet = 1
        sentido = 1 # 1 Derecha, -1 Izquierda
        
        Sun = bpy.data.objects[2]
        Earth = bpy.data.objects[3]
        Mars = bpy.data.objects[4]
        Comet = bpy.data.objects[5]
        
        for i in range(0, R, 1):
            x4 = Comet.location.x
            y4 = Comet.location.y

            x3 = Mars.location.x
            y3 = Mars.location.y
            
            x2 = Earth.location.x
            y2 = Earth.location.y
            
            x1 = Sun.location.x
            y1 = Sun.location.y
            
            radio = math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
            radio2 = math.sqrt( (x3 - x1)**2 + (y3 - y1)**2 )
            radio3 = math.sqrt( (x4 - x2)**2 + (y4 - y2)**2 )
            
            angulo = longitud_arco / radio
            angulo2 = longitud_arco_Mars / radio2
            angulo3 = longitud_arco_Comet / radio3
                    
            angulo = angulo * sentido
            angulo2 = angulo2 * sentido
            angulo3 = angulo3 * sentido

            Earth.location.x = x1 + ( x2 - x1 ) * math.cos(angulo) - ( y2 - y1 ) * math.sin(angulo)
            Earth.location.y = y1 + ( x2 - x1 ) * math.sin(angulo) + ( y2 - y1 ) * math.cos(angulo)
            
            Mars.location.x = x1 + ( x3 - x1 ) * math.cos(angulo2) - ( y3 - y1 ) * math.sin(angulo2)
            Mars.location.y = y1 + ( x3 - x1 ) * math.sin(angulo2) + ( y3 - y1 ) * math.cos(angulo2)
           
            Comet.location.x = x2 + ( x4 - x2 ) * math.cos(angulo3) - ( y4 - y2 ) * math.sin(angulo3)
            Comet.location.y = y2 + ( x4 - x2 ) * math.sin(angulo3) + ( y4 - y2 ) * math.cos(angulo3)
            
            time.sleep(0.01)
            
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1) #Actualiza el estado gráfico para apreciar la animacion

        return {'FINISHED'}            # this lets blender know the operator finished successfully.

def register():
    bpy.utils.register_class(PlanetRotation3)


def unregister():
    bpy.utils.unregister_class(PlanetRotation3)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   