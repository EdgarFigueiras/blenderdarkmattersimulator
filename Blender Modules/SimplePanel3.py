import bpy
import time
from bpy.props import (StringProperty,
                        IntProperty,
                       PointerProperty,
                       )
from bpy.types import (Panel,
                       Operator,
                       AddonPreferences,
                       PropertyGroup,
                       )

# ------------------------------------------------------------------------
#    ui
# ------------------------------------------------------------------------

#Clean the scene
bpy.ops.object.select_by_type(type='MESH')
bpy.ops.object.delete()

class MySettings(PropertyGroup):

    path = StringProperty(
        name="Fichero de datos",
        description="Escoger el fichero con los datos para la simulación",
        default="",
        maxlen=1024,
        subtype='FILE_PATH')

    particle_file_path = StringProperty(
        name="Fichero de partículas",
        description="Ruta en la cual se almacenará el fichero con los datos de las partículas",
        default="",
        maxlen=1024,
        subtype='DIR_PATH')

    image_path = StringProperty(
        name="Ruta donde almacenar renders",
        description="Ruta en la cual se almacenarán los renders",
        default="",
        maxlen=1024,
        subtype='DIR_PATH')

    int_box_n_particulas = IntProperty(
        name="Nº de partículas por cuerpo", 
        description="Numero de partículas por cada uno de los emisores",
        min = 50, max = 500,
        default = 150)
 
class OBJECT_OT_ResetButton(bpy.types.Operator):
    bl_idname = "reset.image"
    bl_label = "Reiniciar entorno"
    country = bpy.props.StringProperty()

    def execute(self, context):
        bpy.ops.object.select_by_type(type='MESH')
        bpy.ops.object.delete()
        bpy.context.scene.frame_current = 0
        return{'FINISHED'} 

class OBJECT_OT_RenderButton(bpy.types.Operator):
    bl_idname = "render.image"
    bl_label = "Renderizar"
    country = bpy.props.StringProperty()

    #This code 
    def execute(self, context):
        dir_image_path = bpy.data.scenes['Scene'].my_tool.image_path
        if dir_image_path == "":
            bpy.data.scenes['Scene'].render.filepath = '/Users/edgarfigueiras/Desktop/image' + time.strftime("%c%s") + '.jpg'
        else:
            bpy.data.scenes['Scene'].render.filepath = dir_image_path + time.strftime("%c%s") + '.jpg'
     
        bpy.ops.render.render( write_still=True ) 
        return{'FINISHED'} 

      


class OBJECT_PT_my_panel(Panel):
    bl_idname = "OBJECT_PT_my_panel"
    bl_label = "Simulation Panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "Tools"
    bl_context = "objectmode"

class Panel(bpy.types.Panel):
    """Panel para añadir al entorno 3D"""
    bl_label = "Simulation Panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'


    def draw(self, context):
        layout = self.layout
        scn = context.scene
        col = layout.column(align=False)

        col.prop(scn.my_tool, "path", text="")

        col.prop(scn.my_tool, "particle_file_path", text="")

        col.prop(scn.my_tool, "int_box_n_particulas")

        col.operator("particle.generator", text="GenerarSimulacion")

        col.operator("particle.colocation", text="ColocarParticulas")

        col.prop(scn.my_tool, "image_path", text="")

        col.operator("render.image", text="Renderizar")

        col.operator("reset.image", text="Reiniciar entorno")

        # print the path to the console
        print (scn.my_tool.path)

# ------------------------------------------------------------------------
#    register and unregister functions
# ------------------------------------------------------------------------

def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.my_tool = PointerProperty(type=MySettings)

def unregister():
    bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.my_tool

if __name__ == "__main__":
    register()

#bpy.utils.register_module()
