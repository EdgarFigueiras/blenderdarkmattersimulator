bl_info = {    
    "name": "Spheres Creation 2",    
    "category": "Object",
}

import bpy
import time
import math

class SpheresCreation2(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.move_x"           # unique identifier for buttons and menu items to reference.
    bl_label = "Spheres Creation 2"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

        #Borrar todos los objetos de la escena
        bpy.ops.object.select_by_type(type='MESH')
        bpy.ops.object.delete()
      
        #Definir un material
        def makeMaterial(name, diffuse, specular, alpha):
            mat = bpy.data.materials.new(name)
            mat.diffuse_color = diffuse
            mat.type = 'HALO'
            mat.diffuse_intensity = 1.0
            mat.specular_color = specular
            mat.specular_intensity = 0.5
            mat.alpha = alpha
            mat.ambient = 1
            return mat
        
        #Creo materiales y los nombro en funcion a su color
        red = makeMaterial('Red',(1,0,0),(1,1,1),1)
        green = makeMaterial('Green',(0,1,0),(1,1,1),1)
        blue = makeMaterial('Blue',(0,0,1),(1,1,1),1)
        white = makeMaterial('White',(1,1,1),(1,1,1),1)
    
        numeroObjeto = 000
        f = open('/Users/edgarfigueiras/Documents/TFG DM/Blender Modules/datos.txt', 'r')
        for line in f:
             
            #Añadiendo los datos del fichero a 3 variables 
            xx,yy,zz = line.split(" ")
            
            #Creando el objeto, plantilla:
            #bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=2, size=1.0, view_align=False, enter_editmode=False, location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0), layers=(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))¶
            ob = bpy.ops.mesh.primitive_uv_sphere_add(size=0.2, location=(float(xx),float(yy),float(zz)))
            
            emitter = bpy.context.object
 
            #bpy.ops.mesh.primitive_uv_sphere_add(size=0.2, location=(float(xx),float(yy),float(zz)))
            #psys1 = bpy.ops.mesh.primitive_uv_sphere_add(size=0.2, location=(float(xx),float(yy),float(zz)))
         
            bpy.ops.object.particle_system_add()    
            psys1 = emitter.particle_systems[-1]
            psys1.name = 'Drops'
            
            # Emission    
            pset1 = psys1.settings
            pset1.name = 'DropSettings'
            pset1.normal_factor = 0.0
            pset1.object_factor = 1
            pset1.factor_random = 0.1
            pset1.frame_start = 0
            pset1.frame_end = 400
            pset1.lifetime = 400
            pset1.lifetime_random = 0.5
            pset1.emit_from = 'FACE'
            pset1.use_render_emitter = True
            pset1.object_align_factor = (0,0,0)
            pset1.count = 1000
         
            # Physics
            pset1.physics_type = 'NEWTON'
            pset1.mass = 0.1
            pset1.particle_size = 10.1
            pset1.use_multiply_size_mass = True
            
            # Effector weights
            ew = pset1.effector_weights
            ew.gravity = 0
            ew.wind = 0
         
            # Children
            pset1.child_nbr = 10
            pset1.rendered_child_count = 10
            pset1.child_type = 'NONE'
         
            # Display and render
            pset1.draw_percentage = 100
            pset1.draw_method = 'CIRC'
            pset1.material = 1
            pset1.particle_size = 0.1    
            pset1.render_type = 'HALO'
            pset1.render_step = 3
            pset1.show_number = False
            pset1.show_size = False
                       
            nombreMesh = "Figura" + str(numeroObjeto)
            me = bpy.data.meshes.new(nombreMesh)
            
            #Debido a que los objetos son nombrados "Sphere.xxx" Donde las x representa el numero
            #Es necesario un codigo para poder acceder a los objetos cada vez que la cifra cambia de
            #digitos. 
            if numeroObjeto==0:            
                nombreObjeto = "Sphere"
                ob = bpy.data.objects.get(nombreObjeto, me)
                ob.active_material = red
            if (numeroObjeto>0 and numeroObjeto<10):
                nombreObjeto = "Sphere.00" + str(numeroObjeto)
                ob = bpy.data.objects.get(nombreObjeto, me)
                ob.active_material = blue
            if (numeroObjeto>=10 and numeroObjeto<100):
                nombreObjeto = "Sphere.0" + str(numeroObjeto)
                ob = bpy.data.objects.get(nombreObjeto, me)
                ob.active_material = white
            
            scn = context.scene      # get the current scene
       
             #Actualiza el estado gráfico para apreciar la animacion          
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)  
            numeroObjeto += 1
            
        #me.from_pydata(verts, [], faces)
        return {'FINISHED'}            # this lets blender know the operator finished successfully.
        f.close()

def register():
    bpy.utils.register_class(SpheresCreation2)


def unregister():
    bpy.utils.unregister_class(SpheresCreation2)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   