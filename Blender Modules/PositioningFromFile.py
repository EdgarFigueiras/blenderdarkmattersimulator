bl_info = {    
    "name": "MovingFromFile",    
    "category": "Object",
}

import bpy
import time
import math

class MovingFromFile(bpy.types.Operator):
    """My Object Moving Script"""         # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.move_x"           # unique identifier for buttons and menu items to reference.
    bl_label = "Moving From File"  # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}     # enable undo for the operator.
   
    def execute(self, context):        # execute() is called by blender when running the operator.

        f = open('/Users/edgarfigueiras/Documents/TFG DM/Blender Modules/datos.txt', 'r')
        # The original script
        scene = context.scene
        Sun = bpy.data.objects[2]
        #R = 32 # Numero total de movimientos 
        #for i in range(0, R, 1):
        for line in f:
            x,y,z = line.split(" ")
            x1 = Sun.location.x
            y1 = Sun.location.y
            z1 = Sun.location.z

            Sun.location.x = float(x)
            Sun.location.y = float(y)
            Sun.location.z = float(z)

            time.sleep(0.01)
            
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1) #Actualiza el estado gráfico para apreciar la animacion

        return {'FINISHED'}            # this lets blender know the operator finished successfully.
        f.close()
def register():
    bpy.utils.register_class(MovingFromFile)


def unregister():
    bpy.utils.unregister_class(MovingFromFile)
    
# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()   