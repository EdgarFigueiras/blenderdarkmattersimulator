import matplotlib.pyplot as mp
import  numpy as np
from scipy import interpolate
from scipy.interpolate import spline
import random


def gaussSimple(x):
    return (1*np.exp(-(x-(-0))**2/(2*2.5**2)))

def gaussJorobas(x):
    return (1*np.exp(-(x-(-5))**2/(2*2.5**2)))+(1*np.exp(-(x-(5))**2/(2*2.5**2)))

def gaussV2(x):
    return (0.35*np.exp(-(x-(-6))**2/(2*2.25**2)))+(1*np.exp(-(x-(5))**2/(2*3**2)))



N=50

x = []
y = []

for z in range(N):
	r = random.uniform(-10,10);
	x.append(r)

x.sort()

for z in range(N):
	y.append(gaussSimple(x[z]))

#Interpolacion recta

f = interpolate.interp1d(x, y)

xnew2 = np.arange(-10, 10, 0.01)
ynew2 = f(xnew2)   # use interpolation function returned by `interp1d`

#mp.subplot(2,1,1)
#mp.plot(x, y, 'o', xnew2, ynew2, '-')


#Spline que suaviza la interpolacion
#Numero que representa el total de samples, cuanto mayor mejor aproximacion, pero en exceso peores resultados
#N1 = 5

#xnew1 = np.linspace(-10,10,N1)

#power_smooth1 = spline(x,y,xnew1)

#Spline que suaviza la interpolacion
#Numero que representa el total de samples, cuanto mayor mejor aproximacion, pero en exceso peores resultados
#N2 = 1000

#xnew12 = np.linspace(-10,10,N2)

#power_smooth12 = spline(x,y,xnew12)

#print(power_smooth12);

#mp.subplot(2,1,2)
#mp.plot(xnew1,power_smooth)

mp.plot(x, y, 'o', xnew1, power_smooth1, '--' , xnew12, power_smooth12, 'k-' )

mp.legend(['data', 'spline N=' + str(N1), 'spline N=' + str(N2)], loc='best')

mp.show()



