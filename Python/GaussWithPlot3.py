from matplotlib import pyplot as mp
import numpy as np
import math

def gaussian(x, mu, sig):
    return 1./(math.sqrt(2.*math.pi)*sig)*np.exp(-np.power((x - mu)/sig, 2.)/2)

def gaussian2(x, mu, sig):
    return 1./(math.sqrt(2.*math.pi)*sig)*np.exp(-np.power((x - mu)/sig, 2.)/2)

#x = gaussian(np.linspace(1, 5, 50), -1, 1)
#y = gaussian2(np.linspace(2, 4, 50), 0, 5)
#z = gaussian2(np.linspace(3, -2, 50), -1, 1)
#mp.plot(y+z)
#mp.plot(gaussian(np.linspace(-3, 0, 200), -1, 1))
#mp.plot(gaussian2(np.linspace(0, 3, 200), -1, 1))

x = np.random.rand(1,100)
y = np.random.rand(1,100)

f = np.exp(-x*x/100)

result = y*f

mp.plot(result)
mp.plot(y)
mp.plot(f)

mp.show()