from matplotlib import pyplot as mp
import numpy as np
import math
import random

# a=>altura del eje X, mu=>centro, sigma=>desviacion
def gauss(x, a, mu, sigma):
    return a*np.exp(-(x-mu)**2/(2*sigma**2))

def gauss(x, a, mu, sigma):
    return a*np.exp(-(x-mu)**2/(2*sigma**2))

# a=>1, mu=>0, sigma=>0.5
def gaussSimple(x):
    return (1*np.exp(-(x-(-0))**2/(2*1**2)))

def gaussJorobas(x):
    return (1*np.exp(-(x-(-5))**2/(2*2.5**2)))+(1*np.exp(-(x-(5))**2/(2*2.5**2)))

def gaussV2(x):
    return (0.35*np.exp(-(x-(-6))**2/(2*2.25**2)))+(1*np.exp(-(x-(5))**2/(2*3**2)))


#Variable con el numero total de iteraciones del bucle para la creacion de los numeros aleatorios
N = 10000

array_resultados = [ ]
array_probabilidad = [ ]

for x in range(N):
	r = random.uniform(-10,10);
	print "-- Punto r = %f Probabilidad = %f --" % (r, gaussV2(r))
	if random.uniform(0, 1) < gaussV2(r):
		array_resultados.append(r)
		array_probabilidad.append(gaussV2(r))

#Grafica con la representacion de la funcion
mp.subplot(2,1,2)
mp.plot(xbar,ybar)
mp.ylim(0,1)
mp.xlabel("Valor")
mp.ylabel("Probabilidad")

#Grafica con el histograma de resultado
xbar = np.linspace(-10, 10, N)			#Definicion de los datos para el eje X
ybar = gaussV2(np.linspace(-10, 10, N)) #Definicion de los datos para el eje Y
mp.subplot(2,1,1)
mp.hist(array_resultados, bins=50, color='red')
mp.xlim(-10,10)
mp.ylabel("Total")


mp.show()




