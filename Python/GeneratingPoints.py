from matplotlib import pyplot as mp
import numpy as np
import math
import random

# a=>altura del eje X, mu=>centro, sigma=>desviacion
def gauss(x, a, mu, sigma):
    return a*np.exp(-(x-mu)**2/(2*sigma**2))

# a=>1, mu=>0, sigma=>2.5
def gaussSimple(x):
    return (1*np.exp(-(x-(-0))**2/(2*25**2)))

def gaussSimpleIzq(x):
    return (1*np.exp(-(x-(-13))**2/(2*6**2)))

def gaussSimpleDer(x):
    return (1*np.exp(-(x-(13))**2/(2*6**2)))

def gaussJorobas(x):
    return (1*np.exp(-(x-(-5))**2/(2*2.5**2)))+(1*np.exp(-(x-(5))**2/(2*2.5**2)))

def gaussV2(x):
    return (0.35*np.exp(-(x-(-6))**2/(2*2.25**2)))+(1*np.exp(-(x-(5))**2/(2*3**2)))

def gaussJorobas2(x):
    return (1*np.exp(-(x-(-10))**2/(2*2.5**2)))+(1*np.exp(-(x-(10))**2/(2*2.5**2)))

def gaussJorobas3(x):
    return (1*np.exp(-(x-(-13))**2/(2*2.5**2)))+(1*np.exp(-(x-(13))**2/(2*6**2)))+(1*np.exp(-(x-(-0))**2/(2*1.25**2)))


#Variable con el numero total de iteraciones del bucle para la creacion de los numeros aleatorios
N = 500

array_resultados = [ ]

f = open('gaussSimple.txt', 'w+')

for x in range(N):
	r = random.uniform(-50,50);
	if random.uniform(0, 1) < gaussSimple(r):
		array_resultados.append(r)
		f.write(str(r) + " " + str(gaussSimple(r)) + "\n")


f.close()


mp.show()




