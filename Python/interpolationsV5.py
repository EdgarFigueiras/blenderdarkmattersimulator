import matplotlib.pyplot as mp
import  numpy as np
from scipy import interpolate
from scipy.interpolate import spline


x = np.array([0,1,2,3,4,5,6])
y = np.array([1,3,5,0,7,3,2])

#Interpolacion recta

f = interpolate.interp1d(x, y)

xnew2 = np.arange(0, 6, 0.01)
ynew2 = f(xnew2)   # use interpolation function returned by `interp1d`

#mp.subplot(2,1,1)
#mp.plot(x, y, 'o', xnew2, ynew2, '-')


#Spline que suaviza la interpolacion
#Numero que representa el total de samples, cuanto mayor mejor aproximacion, pero en exceso peores resultados
N1 = 10

xnew1 = np.linspace(x.min(),x.max(),N1)

power_smooth1 = spline(x,y,xnew1)

#Spline que suaviza la interpolacion
#Numero que representa el total de samples, cuanto mayor mejor aproximacion, pero en exceso peores resultados
N2 = 50

xnew12 = np.linspace(x.min(),x.max(),N2)

power_smooth12 = spline(x,y,xnew12)

#mp.subplot(2,1,2)
#mp.plot(xnew1,power_smooth)

mp.plot(x, y, 'o', xnew2, ynew2, '-', xnew1, power_smooth1, '--' , xnew12, power_smooth12, 'k:' )

mp.legend(['data', 'interpolated', 'spline N=' + str(N1), 'spline N=' + str(N2)], loc='best')

mp.show()



