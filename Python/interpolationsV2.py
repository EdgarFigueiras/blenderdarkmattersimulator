import matplotlib.pyplot as plt
import  numpy as np
from scipy import interpolate
from scipy.interpolate import spline

x = (0,1,2,3,5)
y = (1,3,5,0,7)

#x = np.arange(0, 3, 0.5)
#y = np.exp(-x/3.0)


f = interpolate.interp1d(x, y)

xnew = np.arange(0, 5, 0.01)
ynew = f(xnew)   # use interpolation function returned by `interp1d`

plt.plot(x, y, 'o', xnew, ynew, '-')
plt.show()