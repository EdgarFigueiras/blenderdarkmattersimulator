import matplotlib.pyplot as mp
import  numpy as np
from scipy import interpolate
from scipy.interpolate import spline
import random


def gaussSimple(x):
    return (1*np.exp(-(x-(-0))**2/(2*2.5**2)))

def gaussJorobas(x):
    return (1*np.exp(-(x-(-5))**2/(2*2.5**2)))+(1*np.exp(-(x-(5))**2/(2*2.5**2)))

def gaussV2(x):
    return (0.35*np.exp(-(x-(-6))**2/(2*2.25**2)))+(1*np.exp(-(x-(5))**2/(2*3**2)))

def spline_function(x, y, N):
	x.sort
	y.sort
	f = interpolate.interp1d(x, y)
	for z in range(N):
		r = random.uniform(-10,10);
		x2.append(r)

		x2.sort()

	for z in range(N):
		y2.append(gaussJorobas(x[z]))

	return f


N=50

x = [-1,-0.4,0.6,0,1]
y = [1,1.2,2,1.6,1]

#function = spline_function(x,y)




#Interpolacion recta

f = interpolate.interp1d(x, y)

print(f);

xnew = np.arange(-1, 1, 0.01)
ynew = f(xnew)   # use interpolation function returned by `interp1d`


x2 = []


for z in range(N):
		r = random.uniform(-1,1);
		x2.append(r)

		x2.sort()




#Spline que suaviza la interpolacion
#Numero que representa el total de samples, cuanto mayor mejor aproximacion, pero en exceso peores resultados
#N1 = 10

#xnew1 = np.linspace(-10,10,N1)

#power_smooth1 = spline(x,y,xnew1)

#Spline que suaviza la interpolacion
#Numero que representa el total de samples, cuanto mayor mejor aproximacion, pero en exceso peores resultados
#N2 = 50

#xnew12 = np.linspace(-10,10,N2)

#power_smooth12 = spline(x,y,xnew12)

#print(xnew12);

#mp.subplot(2,1,2)
#mp.plot(xnew1,power_smooth)

mp.plot(x2, f(x2), 'k-' )

mp.legend(['interpolation(x,y)'], loc='best')
mp.show()



