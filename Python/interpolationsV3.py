import matplotlib.pyplot as plt
import  numpy as np
from scipy import interpolate
from scipy.interpolate import spline


x = np.array([0,1,2,3,4,5,6])
y = np.array([1.53E+03, 5.92E+02, 2.04E+02, 7.24E+01, 2.72E+01, 1.10E+01, 4.70E+03])


xnew = np.linspace(x.min(),x.max(),300)

power_smooth = spline(x,y,xnew)

plt.plot(xnew,power_smooth)
plt.show()
