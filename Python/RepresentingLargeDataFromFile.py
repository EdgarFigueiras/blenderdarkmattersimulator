import matplotlib.pyplot as mp
import numpy as np
from scipy import interpolate
from scipy.interpolate import spline
import random

file_with_data = open("gaussSimple.txt", "rw+")

x_data_file = []
y_data_file = []


for line in file_with_data: 
    xData,yData = line.split(" ")  
    x_data_file.append(float(xData))
    y_data_file.append(float(yData))


#Order the two lists mantaining  their correlation
x_data_file, y_data_file = (list(t) for t in zip(*sorted(zip(x_data_file, y_data_file))))

#x_data_file, y_data_file = zip(*sorted(zip(x_data_file, y_data_file)))
#list1, list2 = zip(*sorted(zip(list1, list2)))


#Interpolation straight
funcInter = interpolate.interp1d(x_data_file, y_data_file)

array_resultados = []

N = 50000


for x in range(N):
	r = random.uniform(min(x_data_file),max(x_data_file));
	if random.uniform(0, 1) < funcInter(r):
		array_resultados.append(r)

x2 = []

for z in range(N):
		r = random.uniform(-1,1);
		x2.append(r)

x2.sort()

xbar = np.linspace(-55, 55, N)			     #Definicion de los datos para el eje X
#ybar = funcInter(np.linspace(min(x_data_file), max(x_data_file), N)) #Definicion de los datos para el eje Y
ybar = np.interp(xbar, x_data_file, y_data_file)


mp.subplot(2,1,2)
mp.plot(xbar,ybar, 'k-')
#mp.plot(x2,funcInter(x2), '-')
mp.ylim(0,1)
mp.xlabel("Valor")
mp.ylabel("Probabilidad")

mp.subplot(2,1,1)
mp.hist(array_resultados, bins=30, color='green')
mp.xlim(-55,55)
mp.ylabel("Total")

mp.show()





