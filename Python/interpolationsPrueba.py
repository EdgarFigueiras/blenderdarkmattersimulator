import matplotlib.pyplot as mp
import  numpy as np
from scipy import interpolate
from scipy.interpolate import spline
import random

 # Fit Parameters:
   set.seed(1953)
   x = rnorm(1000)
   param = ssdFit(x)
   
   # Plot empirical density and compare with fitted density:
   hist(x, n = 25, probability = TRUE, border = "white", col = "steelblue") 
   s = seq(min(x), max(x), 0.1)
   lines(s, dssd(s, param), lwd = 2, col = "brown")
   
   # Plot df and compare with true df:
   plot(sort(x), (1:1000/1000), main = "Probability", col = "steelblue")
   lines(s, pssd(s, param), lwd = 2, col = "brown")
   grid()
   
   # Compute quantiles:
   qssd(pssd(seq(-3, 3, 1), param), param) 
