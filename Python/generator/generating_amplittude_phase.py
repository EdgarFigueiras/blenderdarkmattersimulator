#from matplotlib import pyplot as mp
import matplotlib.pyplot as mp
import numpy as np
import math
import random


# a=>altura del eje X, mu=>centro, sigma=>desviacion
def gauss(x, a, mu, sigma):
    return a*np.exp(-(x-mu)**2/(2*sigma**2))


#A few functions wich allows the user to calculate diferent types of gaussians
# a=>1, mu=>0, sigma=>2.5
def gaussSimple(x):
    return (1*np.exp(-(x-(0))**2/(2*5.0**2)))

def gaussSimpleReducida(x):
    return (0.35*np.exp(-(x-(-8))**2/(2*3.5**2)))

def gaussSimpleIzq(x):
    return (1*np.exp(-(x-(-13))**2/(2*6.0**2)))

def gaussSimpleDer(x):
    return (1*np.exp(-(x-(13))**2/(2*6.0**2)))

def gaussJorobas(x):
    return (1*np.exp(-(x-(-5))**2/(2*2.5**2)))+(1*np.exp(-(x-(5))**2/(2*2.5**2)))

def gaussV2(x):
    return (0.35*np.exp(-(x-(-6))**2/(2*2.25**2)))+(1*np.exp(-(x-(5))**2/(2*3**2)))

def gaussJorobas2(x):
    return (1*np.exp(-(x-(-10))**2/(2*2.5**2)))+(1*np.exp(-(x-(10))**2/(2*2.5**2)))

def gaussJorobas3(x):
    return (1*np.exp(-(x-(-13))**2/(2*2.5**2)))+(1*np.exp(-(x-(13))**2/(2*6**2)))+(1*np.exp(-(x-(-0))**2/(2*1.25**2)))


#Variable con el numero total de iteraciones del bucle para la creacion de los numeros aleatorios
N = 100  #Number of cicles that will be completed at the "for"
n = -50  #Number from where the generation starts, in this case starts from the -50 point and will go to: -50 + N => -50 + 100 = 50
x = 0    #Variable that will work as the reference to fill the vectors from 0 to 99

#Initializing the twelve arrays that will store the information
array_resultadosXd_amplitud = np.empty(100, dtype=float)
array_resultadosXp_amplitud = np.empty(100, dtype=float)
array_resultadosYd_amplitud = np.empty(100, dtype=float)
array_resultadosYp_amplitud = np.empty(100, dtype=float)
array_resultadosZd_amplitud = np.empty(100, dtype=float)
array_resultadosZp_amplitud = np.empty(100, dtype=float)

array_resultadosXd_fase = np.empty(100, dtype=float)
array_resultadosXp_fase = np.empty(100, dtype=float)
array_resultadosYd_fase = np.empty(100, dtype=float)
array_resultadosYp_fase = np.empty(100, dtype=float)
array_resultadosZd_fase = np.empty(100, dtype=float)
array_resultadosZp_fase = np.empty(100, dtype=float)


f = open('data1_amplitude_phase', 'wb+') #Remember open it using wb, this b means that the file will be a binary file

for x in range(N):
    #X
    array_resultadosXd_amplitud[x]=n
    array_resultadosXp_amplitud[x]=gaussSimple(n)
    #Y
    array_resultadosYd_amplitud[x]=n
    array_resultadosYp_amplitud[x]=gaussSimple(n)
    #Z
    array_resultadosZd_amplitud[x]=n
    array_resultadosZp_amplitud[x]=gaussSimple(n)


    #X fase
    array_resultadosXd_fase[x]=n
    array_resultadosXp_fase[x]=gaussSimpleReducida(n)
    #Y fase
    array_resultadosYd_fase[x]=n
    array_resultadosYp_fase[x]=gaussSimpleReducida(n)
    #Z fase
    array_resultadosZd_fase[x]=n
    array_resultadosZp_fase[x]=gaussSimpleReducida(n)

    n=n+1  
    x=x+1


np.savez(f,array_resultadosXd_amplitud ,array_resultadosXp_amplitud ,array_resultadosYd_amplitud ,array_resultadosYp_amplitud ,array_resultadosZd_amplitud ,array_resultadosZp_amplitud, array_resultadosXd_fase ,array_resultadosXp_fase ,array_resultadosYd_fase ,array_resultadosYp_fase ,array_resultadosZd_fase ,array_resultadosZp_fase)

mp.plot(array_resultadosXd_amplitud, array_resultadosXp_amplitud)

mp.plot(array_resultadosXd_fase, array_resultadosXp_fase)

mp.axis([-20, 20, 0, 1])

mp.show()

f.close()






