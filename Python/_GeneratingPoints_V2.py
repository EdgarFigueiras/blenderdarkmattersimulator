#from matplotlib import pyplot as mp
import numpy as np
import math
import random


# a=>altura del eje X, mu=>centro, sigma=>desviacion
def gauss(x, a, mu, sigma):
    return a*np.exp(-(x-mu)**2/(2*sigma**2))


#A few functions wich allows the user to calculate diferent types of gaussians
# a=>1, mu=>0, sigma=>2.5
def gaussSimple(x):
    return (1*np.exp(-(x-(15))**2/(2*6**2)))

def gaussSimpleIzq(x):
    return (1*np.exp(-(x-(-13))**2/(2*6**2)))

def gaussSimpleDer(x):
    return (1*np.exp(-(x-(13))**2/(2*6**2)))

def gaussJorobas(x):
    return (1*np.exp(-(x-(-5))**2/(2*2.5**2)))+(1*np.exp(-(x-(5))**2/(2*2.5**2)))

def gaussV2(x):
    return (0.35*np.exp(-(x-(-6))**2/(2*2.25**2)))+(1*np.exp(-(x-(5))**2/(2*3**2)))

def gaussJorobas2(x):
    return (1*np.exp(-(x-(-10))**2/(2*2.5**2)))+(1*np.exp(-(x-(10))**2/(2*2.5**2)))

def gaussJorobas3(x):
    return (1*np.exp(-(x-(-13))**2/(2*2.5**2)))+(1*np.exp(-(x-(13))**2/(2*6**2)))+(1*np.exp(-(x-(-0))**2/(2*1.25**2)))


#Variable con el numero total de iteraciones del bucle para la creacion de los numeros aleatorios
N = 100  #Number of cicles that will be completed at the "for"
n = -50  #Number from where the generation starts, in this case starts from the -50 point and will go to: -50 + N => -50 + 100 = 50
x = 0    #Variable that will work as the reference to fill the vectors from 0 to 99

#Initializing the six arrays that will store the information
array_resultadosXd = np.empty(100, dtype=float)
array_resultadosXp = np.empty(100, dtype=float)
array_resultadosYd = np.empty(100, dtype=float)
array_resultadosYp = np.empty(100, dtype=float)
array_resultadosZd = np.empty(100, dtype=float)
array_resultadosZp = np.empty(100, dtype=float)

f = open('datos1', 'wb+') #Remember open it using wb, this b means that the file will be a binary file

for x in range(N):
    #X
    array_resultadosXd[x]=n
    array_resultadosXp[x]=gaussSimple(n)
    #Y
    array_resultadosYd[x]=n
    array_resultadosYp[x]=gaussSimple(n)
    #Z
    array_resultadosZd[x]=n
    array_resultadosZp[x]=gaussSimple(n)


    n=n+1  
    x=x+1


np.savez(f,array_resultadosXd,array_resultadosXp,array_resultadosYd,array_resultadosYp,array_resultadosZd,array_resultadosZp)

f.close()






