import matplotlib.pyplot as mp
import  numpy as np
from scipy import interpolate
from scipy.interpolate import spline


x = np.array([0,1,2,3,4,5,6])
y = np.array([1,3,5,0,7,3,2])

#Interpolacion recta

f = interpolate.interp1d(x, y)

xnew2 = np.arange(0, 6, 0.01)
ynew2 = f(xnew2)   # use interpolation function returned by `interp1d`

mp.subplot(2,1,1)
mp.plot(x, y, 'o', xnew2, ynew2, '-')

#Spline que suaviza la interpolacion

xnew1 = np.linspace(x.min(),x.max(),300)

power_smooth = spline(x,y,xnew1)

mp.subplot(2,1,2)
mp.plot(xnew1,power_smooth)


mp.show()



