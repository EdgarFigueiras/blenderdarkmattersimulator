import matplotlib.pyplot as mp
import  numpy as np
from scipy import interpolate
from scipy.interpolate import spline
import random

x = [-10,-6,-4,0,4,6,10]
y = [0.1,1,1,0.3,1,1,0.1]




#Interpolation straight
funcInter = interpolate.interp1d(x, y)

array_resultados = []

N = 10000


for x in range(N):
	r = random.uniform(-10,10);
	if random.uniform(0, 1) < funcInter(r):
		array_resultados.append(r)

x2 = []

for z in range(N):
		r = random.uniform(-1,1);
		x2.append(r)

x2.sort()


xbar = np.linspace(-10, 10, N)			     #Definicion de los datos para el eje X
ybar = funcInter(np.linspace(-10, 10, N)) #Definicion de los datos para el eje Y


mp.subplot(2,1,2)
mp.plot(xbar,ybar)
mp.plot(x2,funcInter(x2), 'k-')
#mp.ylim(0,1)
mp.xlabel("Valor")
mp.ylabel("Probabilidad")

mp.subplot(2,1,1)
mp.hist(array_resultados, bins=30, color='red')
mp.xlim(-10,10)
mp.ylabel("Total")

mp.show()





