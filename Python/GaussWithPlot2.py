from matplotlib import pyplot as mp
from math import *
import numpy as np

def phi(x):
    #'Cumulative distribution function for the standard normal distribution'
    return (1.0 + erf(x / sqrt(2.0))) / 2.0

def gaussian(x, mu, sig):
    return np.exp(-np.power(x+2 - mu, 2.0) / (2 * np.power(sig, 2.0)))

for mu, sig in [(-1, 1), (0, 2), (2, 3)]:
    mp.plot(gaussian(np.linspace(-3, 3, 120), mu, sig))

mp.show()